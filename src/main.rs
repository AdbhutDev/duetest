use freetype::Library;
use std::env;
use std::fs;
use std::process::exit;
use std::time::Instant;

const ITER_COUNT: i32 = 5000;
const SIZES: [f32; 6] = [5.0, 10.0, 20.0, 50.0, 100.0, 200.0];

fn main() {
  // Get command line arguments
  let args: Vec<String> = env::args().collect();

  // Check if font location is provided
  if args.len() < 2 {
    println!("Usage: duetest [font]");
    exit(0);
  }

  // Alernative way
  //let ft = include_bytes!("../Ubuntu-Regular.ttf") as &[u8];

  // Load the font file in memory
  let ft = &fs::read(&args[1]).unwrap() as &[u8];

  // Strings for output
  let mut out_due = String::from("fontdue\n");
  let mut out_ft = String::from("freetype\n");

  // Fontdue
  // Execute loop for SIZES array
  for fontsize in SIZES.iter() {
    // Store time before execution
    let before_fd = Instant::now();

    // Parse font file into the font type
    let font = fontdue::Font::from_bytes(ft, fontdue::FontSettings::default()).unwrap();

    // Rasterize and get the layout metrics for lowercase english alphabet.
    let mut len_fd = 0 as i64;

    for _ in 0..ITER_COUNT {
      for c in b'a'..=b'z' {
        let (_, bitmap) = font.rasterize(c as char, *fontsize);
        len_fd += bitmap.len() as i64;
      }
    }

    // Store the output in string
    let c = format!(
      "{:5.1} : {:.2?} , Len: {} \n",
      fontsize,
      before_fd.elapsed(), // Calculate execution time
      len_fd
    );
    out_due.push_str(&c);

    // FreeType
    // Store time before execution
    let before_ft = Instant::now();

    // Initialize FreeType from in-memory font
    let lib = Library::init().unwrap();
    let font = ft.to_vec();
    let face = lib.new_memory_face(font, 0).unwrap();

    // Set font size
    face
      .set_char_size(0, (*fontsize * 64.0) as isize, 72, 72)
      .unwrap();

    // Rasterize and get the layout metrics for lowercase english alphabet.
    let mut len_ft = 0 as i64;
    for _ in 0..ITER_COUNT {
      for c in b'a'..=b'z' {
        face
          .load_char(
            c as usize,
            freetype::face::LoadFlag::RENDER | freetype::face::LoadFlag::NO_HINTING, // Disable hinting
          )
          .unwrap();

        let glyph = face.glyph();
        let bitmap = glyph.bitmap();
        len_ft += (bitmap.width() as i64) * (bitmap.rows() as i64);
      }
    }

    // Store the output in string
    let c = format!(
      "{:5.1} : {:.2?} , Len: {} \n",
      fontsize,
      before_ft.elapsed(),
      len_ft
    );
    out_ft.push_str(&c);
  }

  // Finally print the performance metrics
  println!("{}", out_due);
  println!("{}", out_ft);
}
